from django.db import models

def user_directory_path(instance, filename):
    # file will be uploaded to MEDIA_ROOT/user_<id>/<filename>
    return ('user_%s/%s' %(instance.user.id, filename))

class File(models.Model):

    id = models.IntegerField(primary_key=True)
    id_user = models.IntegerField()
    user = models.TextField()
    uploaded_at = models.DateTimeField(auto_now_add=True)
    file_upload = models.FileField(upload_to='files/')
    file_size = models.IntegerField()
    file_name = models.TextField()
