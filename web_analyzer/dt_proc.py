import scipy as sp
import json

def get_vls(file):

    data = sp.genfromtxt(file,delimiter='\t')
    x = data[:,0]
    y = data[:,1]

    x = x[~sp.isnan(y)]
    y = y[~sp.isnan(y)]

    fp1, residuals, rank, sv, rcond = sp.polyfit(x, y, 1, full=True)
    fp53, residuals1, rank1, sv1, rcond = sp.polyfit(x, y, 53, full=True)
    a = json.dumps(x.tolist())
    b = json.dumps(y.tolist())
    # print (x.tolist(), y)
    print (fp1, residuals)
    print(fp53, residuals1, rank1, sv1, rcond)

    return x.tolist(), y.tolist(), fp1
