from django import forms
from .models import File

# class UploadFileForm(forms.ModelForm):
#
#     class Meta:
#
#         model = File
#         fields = ('file_upload',)

class UploadFileForm(forms.Form):

    title = forms.CharField(max_length=50)
    file = forms.FileField()
