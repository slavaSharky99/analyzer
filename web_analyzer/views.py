from django.shortcuts import render, render_to_response, redirect
from django.contrib.auth.decorators import login_required
from django.http import JsonResponse

from .forms import UploadFileForm
from .tools import uploaded_file, get_user_files, create_connect, select_file
from analyzer.settings import MEDIA_ROOT
from .dt_proc import get_vls

@login_required
def index(request):

    print ('index')
    return render(request, 'main.html')

@login_required
def files(request):

    print('files')
    id_user = request.user.id
    files = get_user_files(id_user)
    return render(request, 'files.html', {'files':files})


@login_required
def upload_files(request):

    print(request.FILES, request.user.username)
    if request.POST:
        print('ok')

        connect, cursor = create_connect()

        b = uploaded_file(request.FILES['file'], request.user, cursor)

        connect.commit()
        connect.close()

        if  b:
            print('YEEEES')

            return JsonResponse({'data':'ok', 'is_valid': True})
        else:
            return JsonResponse({'data':'Bad', 'is_valid': False})

@login_required
def chart(request, args):

    if request.POST:

        print(request.POST, args)
        print( select_file(args,request.user.id))
        x, y , fp1 = get_vls( select_file(args,request.user.id))
        
        return JsonResponse({'x':x, 'y':y})

    return render(request, 'chart.html')
