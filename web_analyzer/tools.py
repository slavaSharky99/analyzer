from analyzer.settings import MEDIA_ROOT, DATABASES
import os
from datetime import datetime
from psycopg2 import connect

def create_connect():

    conn = connect(
                        dbname = DATABASES['default']['NAME'],
                        user = DATABASES['default']['USER'],
                        password = DATABASES['default']['PASSWORD'],
                        host = DATABASES['default']['HOST']
                        )
    cursor = conn.cursor()
    return conn, cursor

def rewrite_path(dir):

    ret_str = ''
    for c in dir:

        if c == '\\':
            c = '/'
        ret_str += c

    return ret_str

def get_user_files(id):

    conn, cursor = create_connect()
    select = "SELECT files.id, files.path_file, files.name_file,\
            files.type_file, files.size FROM public.files WHERE files.id_user=%s;"%(id)
    # print(select)
    cursor.execute(select)
    all_files = [] # ret matrix of [['fold_i',[{},{},...]],[]]


    for d in cursor.fetchall():

        buff = {
                'id':d[0],
                'path':d[1],
                'name':d[2],
                'type':d[3],
                'size':size_str(d[4])
            }

        print (buff)

        if len(all_files) == 0:
            all_files.append({"folder":d[1],"files":[buff]})
        else:
            i = 0
            for f in all_files:

                if d[1] == f["folder"]:
                    all_files[i]['files'].append(buff)
                else:
                    all_files.append({"folder":d[1],"files":[buff]})
                i += 1



    cursor.close()
    conn.close()

    return {'files':all_files}

def split_name(name_file):

    i = 0
    h = 0

    for c in name_file:

        if c == '.':
            i += 1
        if c == '\\':
            h += 1

    k = 0
    j = 0

    t = 0
    l = 0

    for s in name_file:
        if k < i:
            j +=1
        if s == '.':
            if k < i:
                k += 1
        if t < h:
            l += 1
        if s == '\\':
            if t < h:
                t += 1
    path = name_file[0:(l-1)]
    name = name_file[l:(j-1)]
    ext = name_file[j:len(name_file)]
    return name, ext, path

def size_str(size):

    size_info = ''
    if size < 1024:
        size_info = str(size) + ' ' + 'Байт'
    elif (size > 1024) and (size < 1024*1024):
        size_info =  str(size // 1024) + ' '+ 'КБайт'
    elif (size > 1024*1024) and (size < 1024*1024*1024):
        size_info = str(size // (1024*1024))+' '+ 'МБайт'
    else:
        size_info = str(size // (1024*1024*1024))+' '+ 'ГБайт'

    return size_info

def select_file(id, user_id):

    conn, cursor = create_connect()
    select = "SELECT files.path_file, files.name_file,\
            files.type_file FROM public.files\
            WHERE files.id_user=%s AND files.id=%s;"%(user_id, id)
    cursor.execute(select)

    fl = cursor.fetchone()

    return fl[0]+'/'+fl[1]+'.'+fl[2]

def uploaded_file(up_file, user, cursor):

    path_user = user.username

    date = datetime.now()
    str_date = date.strftime('%d-%m-%Y')
    print(date)
    if not os.path.exists(MEDIA_ROOT+ '\\'+path_user + '\\' + str_date):
        os.makedirs(MEDIA_ROOT+ '\\'+path_user + '\\' + str_date)

    file_path = MEDIA_ROOT+ '\\'+path_user+ '\\' + str_date + '\\'+up_file.name
    with open(file_path, 'wb+') as destination:
        for chunk in up_file.chunks():
            destination.write(chunk)
    name, ext, path = split_name(file_path)

    print(path)
    insert = """
        INSERT INTO files
        (id_user, path_file, name_file, type_file, date_upload, size)\
        VALUES (\'{}\', \'{}\',\'{}\', \'{}\', \'{}\', \'{}\')
        """.format(user.id, str(rewrite_path(path)), name, ext,
         datetime.strftime(date,"%d.%m.%Y %H:%M:%S"),os.path.getsize(file_path))

    cursor.execute(insert)

    cursor.close()
    return True
