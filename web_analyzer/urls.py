from django.conf.urls import include, url
from django.contrib import admin
from . import views

from web_analyzer.views import index
from web_analyzer.views import files
from web_analyzer.views import upload_files
from web_analyzer.views import chart

urlpatterns = [

    url(r'^index/$', index, name = 'index'),
    url(r'^files/$', files, name = 'files'),
    url(r'^upload_files/$', upload_files, name = 'upload_files'),
    url(r'^chart/(\d+)/$', chart, name = 'chart')
]
