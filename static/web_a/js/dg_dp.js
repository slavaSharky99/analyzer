$(document).ready(function()
{
  $('#form_').on('dragover',function ()
  {
      $(this).addClass('file_drag_over');
      return false;
  });
  $('#form_').on('dragleave',function ()
  {
      $(this).removeClass('file_drag_over');
      return false;
  });
  $('#form_').on('drop',function(e)
  {
      e.preventDefault();
      $(this).removeClass('file_drag_over');
  });
});

function getCookie(name) {
   var cookieValue = null;
   if (document.cookie && document.cookie !== '') {
       var cookies = document.cookie.split(';');
       for (var i = 0; i < cookies.length; i++) {
           var cookie = jQuery.trim(cookies[i]);
           // Does this cookie string begin with the name we want?
           if (cookie.substring(0, name.length + 1) === (name + '=')) {
               cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
               break;
           }
       }
   }
   return cookieValue;
}
$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
            // Only send the token to relative URLs i.e. locally.
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    }
});

$(function () {

  $(".js-upload-photos").click(function () {
    $("#fileupload").click();
  });

  $("#fileupload").fileupload({
    dataType: 'json',
    done: function (e, data) {
      if (data.result.is_valid) {
        console.log(data.result.data);
      }
      console.log(1);
    }
  });

});
