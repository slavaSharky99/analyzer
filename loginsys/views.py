from django.shortcuts import render, render_to_response, redirect
from .forms import RegForm
from django.contrib import auth
from django.template.context_processors import csrf
from django.contrib.auth.forms import UserCreationForm

def login(request):
    args = {}
    args.update(csrf(request))
    args['form'] = RegForm()

    if request.POST:

        login = request.POST.get('username', '')
        password = request.POST.get('password', '')
        print( login, password)
        user = auth.authenticate( username = login, password = password)
        if user is not None:

            auth.login(request, user)
            
            return redirect('/analyzer/index')
        else:
            args['login_error'] = 'Not Found User'
            print('Not User')
            return render_to_response('login.html', args)
    else:

        return render_to_response('login.html', args)

def logout(request):

    auth.logout(request)
    return redirect('/auth/login')

def registration(request):

    args = {}
    args.update(csrf(request))

    if request.POST:
        new_user_form = RegForm(request.POST)
        print(request.POST , new_user_form.is_valid())

        if new_user_form.is_valid():
            new_user_form.save()
            new_user = auth.authenticate(
                        username = new_user_form.cleaned_data['username'],
                        password = new_user_form.cleaned_data['password1']
                        )
            auth.login(request, new_user)
            return redirect('/analyzer/index')
        else:
            args['form'] = new_user_form
    else:
        return render_to_response('login.html', args)
